
import { expect } from 'chai';
/**
 * Build the controller for testing
 */
import 'reflect-metadata';
import {Injector} from 'angular2/core';
import {HTTP_PROVIDERS} from 'angular2/http';

import Controller from '../passit/controller/controller';
import Master from 'simple-asymmetric-javascript';
import Db from '../passit/controller/db';
import User from '../passit/controller/user';
import Api from '../passit/controller/api';
import Message from '../passit/controller/message';

var injector = Injector.resolveAndCreate([Controller, Master, Db, User, Api, Message, HTTP_PROVIDERS]);

var testController = injector.get(Controller);

export default testController;
