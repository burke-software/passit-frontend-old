FROM node:latest
RUN mkdir /code
WORKDIR /code
RUN mkdir ./dist
RUN mkdir ./node_modules
COPY ./package.json /code/package.json
RUN npm install -g tsc
RUN npm install
WORKDIR /code/node_modules/simple-asymmetric-javascript
RUN /code/node_modules/simple-asymmetric-javascript/install.sh
WORKDIR /code
RUN cp -Rv /code/node_modules/simple-asymmetric-javascript/dist/* /code/dist
VOLUME /code/dist
VOLUME /code/node_modules
RUN npm install -g karma
RUN npm install -g webpack
RUN npm install -g webpack-dev-server
