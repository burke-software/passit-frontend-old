
/**
 * The file is used for the definitions for the vars in passit
 */

/**
 * The nedb class
 * @type {nedb}
 */
declare var Nedb:any;

/**
 * Jquery
 * @type {jQuery}
 */
declare var $: any;

/**
 * Awesomecomplete
 * @type {Awesomecomplete}
 */
declare var Awesomplete: any;

/**
 * The user info
 */
interface userInfo {
    /**
     * The email of the user
     * @type {string}
     */
    email:string

    /**
     * The users password
     * @type {string}
     */
    password: string

    /**
     * The token for connecting to the api
     * @type {string}
     */
    token: string,

    /**
     * The current mode of the system online, offline
     * @type {string}
     */
    mode: string,

    /**
     * The users id
     * @type {number}
     */
    id: number,

    /**
     * The number of days we are saving the cookie for
     * @type {number}
     */
    days: number

}

/**
 * Placeholder fields for the login and signup
 */
interface placeHolder {
    /**
     * The email placeholder
     * @type {string}
     */
    email: string,
    
    /**
     * The password placeholder
     * @type {string}
     */
    password: string

}

/**
 * The template object for the message templates
 */
interface messageTemplateObject {
    /**
     * The title of the message
     * @type {string}
     */
    title: string,

    /**
     * The body of the message
     * @type {string}
     */
    body: string,

    /**
     * The current id of the modal to prevent colisions
     * @type {string}
     */
    modalId: string,

    /**
     * If the message can be closed by the user
     * @type {boolean}
     */
    close?: boolean

}

/**
 * The accessgroupuser_set from the groups
 */
interface groupUsers {

    /**
     * The users group id
     * @type {number}
     */
    id: number,

    /**
     * The users id
     * @type {number}
     */
    user: number,

    /**
     * @TODO what is this
     * @type {number}
     */
    access_group: number,

    /**
     * If the user is a group admin
     * @type {boolean}
     */
    is_group_admin: boolean

}

/**
 * The secret to be sent to the server
 */
interface newSecret {

    /**
     * The name of the secret
     * @type {string}
     */
    name: string,

    /**
     * The type of the secret
     * @type {string}
     */
    type: string,

    /**
     * The attribute data
     * @type {object}
     */
    visible_data: any,


    /**
     * The secrets and their data info
     * @type {secretData}
     */
    secrets: secretData[]


}

/**
 * The secrets data
 */
interface secretData {
    
    /**
     * The group id
     * @type {number}
     */
    group?: number,

    /**
     * The encrypted data
     * @type {object}
     */
    hidden_data: any,

    /**
     * The aes key
     * @type {string}
     */
    encrypted_key: string

}
