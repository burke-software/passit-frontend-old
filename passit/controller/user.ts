
import {Injectable} from 'angular2/core';
import {createCookie, getCookie} from '../include/cookie';
import Master from 'simple-asymmetric-javascript';
import Db from './db';

@Injectable()
/**
 * The class for storing the user information
 */
export default class User {

    /**
     * The users info
     * @type {userInfo}
     */
    private _stat: userInfo;

    /**
     * The default stat value
     * @type {userInfo}
     */
    private userDefaults: userInfo = {
        email: '',
        password: '',
        token: '',
        mode: '',
        id: 0,
        days: 0
    };

    /**
     * Get the users info
     * @return {userInfo} a clone of the stat object
     */
    public get stat(): userInfo {
        return Object.create(this._stat);
    }

    /**
     * Users info cannot be set from the outside
     */
    public set stat(obj:userInfo) {

    }

    /** 
    * @param {Master} asym - the class for encryption and decrytpion
    * @param {Db} db - the local datastore of the secrets and groups
    */
    constructor(private _asym:Master, private _db:Db){
        this._load();
        if(this.authenticated()){
            this.load_keys();
        } else {
            //clear local storage
            this.clear();
        }
    }

    /**
     * Check if the user is authenticated
     * @return {boolean} true is the user is authenticated
     */
    public authenticated(): boolean {
        if(this._stat.password !== ''){
            return true;
        }
        return false;
    }

    /**
     * Save the public and private key to disk
     * @param {string} public_key - the public key as a pem file
     * @param {string} private_key - the encrypted private key pem file
     */
    public save_keys(public_key:string, private_key:string): void {
        localStorage.setItem('user_keys', JSON.stringify({public_key: public_key, private_key: private_key}));
        this._asym.exec('set_public_key', {public_key: public_key});
        this._asym.exec('set_private_key', {private_key: private_key, passphrase: this._stat.password});
    }

    /**
     * Load the users keys from disk
     */
    public load_keys(): void {
        var keys = JSON.parse(localStorage.getItem('user_keys'));
        this._asym.exec('set_public_key', {public_key: keys.public_key});
        this._asym.exec('set_private_key', {private_key: keys.private_key, passphrase: this._stat.password});
    }

    /**
     * Get a value from the stat
     * @param {string} name - the name of the property
     * @return {any} - the value
     */
    public get(name:string): any {
        return this._stat[name];
    }

    /**
     * Set a value on the stat
     * @param {string} name - the name of the property
     * @param {any} value - the value of the property
     */
    public set(name:string, value:any): void {
        this._stat[name] = value;
    }


    /**
     * Load the users info from a cookie
     */
    private _load(): void {
        var data = getCookie('user');
        if(!data){
            this._stat = JSON.parse(JSON.stringify(this.userDefaults));
        } else {
            this._stat = JSON.parse(atob(data));
        }
    }

    /**
     * Save the user info to a cookie
     * @param {number|null} days - the number of days to save the cookies or none for a session cookie
     */
    public save(days?:number): void {
        if(this._stat){
            var data = btoa(JSON.stringify(this._stat));
            createCookie('user', data, days);
        }
    }

    /**
     * Delete the users cookie
     */
    public deleteCookie(): void {
        createCookie('user','', -1);
    }

    /**
     * Clear the users cookies and localStorage
     * @return {Promise} the db destory promise
     */
    public clear(): any {
        localStorage.clear();
        this._stat = JSON.parse(JSON.stringify(this.userDefaults));
        this.deleteCookie();
        return this._db.destroy();
    }

}


