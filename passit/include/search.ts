
import {Control, ControlGroup, Validators} from 'angular2/common';

/**
 * The search control
 * @type {Control}
 */
export var searchControl: Control = new Control('', Validators.required);

/**
 * The search group
 * @type {ControlGroup}
 */
export var searchGroup: ControlGroup = new ControlGroup({
    search: this.searchControl
});
