
import {Control, ControlGroup, Validators} from 'angular2/common';
import {emailValidator} from './validator';

/**
 * The form control for the login and signup
 */

/**
 * The form control for the email
 * @type {Control}
 */
export var emailControl: Control = new Control('', emailValidator);

/**
 * The users password
 * @type {Control}
 */
export var passwordControl: Control = new Control('', Validators.minLength(6));

/**
 * The remember me button
 * @type {Control}
 */
export var rememberMeControl = new Control(false);

/**
 * The signup/login form group
 * @type {ControlGroup}
 */
export var formGroup: ControlGroup = new ControlGroup({
    email: emailControl,
    password: passwordControl,
    rememberMe: rememberMeControl
});

export var placeholder: placeHolder = {
    email: 'Your Email',
    password: 'Your Password'
};
