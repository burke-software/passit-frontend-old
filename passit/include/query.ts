
/**
 * Parse a url query
 * @param {string} query - the url query
 * @return {object} the query as an object
 */
export var parseQuery = (query:string): any =>{
    //remove the #
    query = query.substr(1);
    var result = {};
    query.split("&").forEach(function(part) {
        var item = part.split("=");
        result[item[0]] = decodeURIComponent(item[1]);
    });
    return result;
};
