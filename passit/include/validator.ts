
import {Control, Validators} from 'angular2/common';

/**
 * Custom Validators for the app
 */

/**
 * Validate a users email
 * @param {Control} control - the control to validate
 * @return {boolean} if the email is valid
 */
export var emailValidator = (control: Control): {[key: string]: boolean} => {
    var re: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(control.value) ? null : {"Invalid Email": true};
};

/**
 * Validate a group slug, no special charaters, no uppercase and no spaces
 * @param {Control} control - the slug to validate
 * @return {boolean} if the slug is vaid
 */
export var groupSlugValidator = (control:Control): {[key:string]: boolean} =>{
    if(control.value === ''){
       return {"Invalid Group Slug":true}; 
    }
    var re: RegExp = /^.*[A-Z !@#$%^&*()_+=\[\]{};':"\\|,.<>\/?].*$/;
    return re.test(control.value) ? {"Invalid Group Slug":true} : null;
};

/**
 * Validate a url
 * @param {Control} control - the control to validate
 * @return {boolean} if the url is valid
 */
export var urlValidator = (control:Control): {[key:string]: boolean} =>{
    var re: RegExp = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;
    return re.test(control.value) ? null : {"Invalid Url": true};
};
