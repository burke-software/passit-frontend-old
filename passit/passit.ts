
import {Component, OnInit} from 'angular2/core';
import {Router, RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import Controller from './controller/controller';
import Login from './routes/login';
import Signup from './routes/signup';
import Home from './routes/home';

@Component({
    selector:'app',
    template: require('./template/passit.html'),
    directives:[ROUTER_DIRECTIVES]
})
@RouteConfig([
    { path:'/login', component:Login, name:'Login', useAsDefault: true },
    { path:'/signup', component:Signup, name:'Signup'},
    { path:'/home/...', component:Home, name:'Home'},
])
export default class Passit implements OnInit {

    /**
     * The user info to pass to the template
     * @type {userInfo}
     */
    public stat: userInfo = this._controller.user.stat;

    constructor(private _router:Router, private _controller:Controller){

    }

    ngOnInit(){
        if(!this._controller.user.authenticated()){
            this._router.navigate(['/Login']);
        }
    }

    /**
     * Log the user out
     */
    public logout(): void {
        this._controller.user.clear().then(()=>{
            this.stat = this._controller.user.stat;
            this._router.navigate(['/Login']);
        });
    }

};
