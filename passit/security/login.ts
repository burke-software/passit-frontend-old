
import Controller from '../controller/controller';

/**
 * Log the user in
 * @param {Controller} controller - the app controller
 * @param {string} email - the users email
 * @param {string} password - the users password
 * @param {boolean} rememberMe - if true remeber the user beyond the session
 * @return {Promise}
 */
export var login = (controller:Controller, email:string, password: string, rememberMe: boolean): any =>{
    return new Promise((resolve, reject)=>{
        //get the users salt
        controller.api.jsonGet(`/api/user-public-auth/${email}`).subscribe(res =>{
            res = res.json();
            //create the users hash
            controller.asym.exec('generate_hash', {password: password, numIterations: res.client_iterations, salt: res.client_salt}).then((hashObj)=>{
                //send the hash to the server
                controller.api.login('/api/auth/login/', email, hashObj.hash).subscribe(res =>{
                    res = res.json();
                    controller.user.set('token', res.token);
                    controller.user.set('email', res.user.email);
                    controller.user.set('password', password);
                    controller.user.set('mode', 'online');
                    controller.user.set('id', res.user.id);
                    controller.user.save_keys(res.user.public_key, res.user.private_key);
                    if(rememberMe){
                        controller.user.set('days', 30);
                        controller.user.save(30);
                    } else {
                        controller.user.save();
                    }
                    return resolve();
                }, err =>{
                    return reject();
                });
            });
        }, err =>{
            return reject();
        });
    });
};
