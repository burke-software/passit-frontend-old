
import Controller from '../controller/controller';

/**
 * Create a new group
 * @param {Controller} controller - the app controller
 * @param {string} name - the group name
 * @param {string} slug - the group slug
 * @return {Promise} when complete resolve
 */
export var createGroup = (controller:Controller, name:string, slug:string): any =>{
    return new Promise((resolve, reject)=>{
        /**
         * The users public key in an object
         * @type {object}
         */
        var publicKey;
        /**
         * The group public and private key
         * @type {object}
         */
        var keys;
        /**
         * The group aes key
         * @type {object}
         */
        var aesKey;
        //get the users public key
        controller.asym.exec('public_key').then((publicKeyObj)=>{
            publicKey = publicKeyObj;
            //generate the group keys
            return controller.asym.exec('make_rsa_keys');
        }).then((keyObj)=>{
            //reload the users keys
            controller.user.load_keys();
            keys = keyObj;
            //make a new group aes key
            return controller.asym.exec('make_aes_key');
        }).then(()=>{
            //encrypt the aes key with the users public key
            return controller.asym.exec('get_encrypted_aes_key', {public_key: publicKey.public_key, use_base64:true});
        }).then((aesKeyObj)=>{
            aesKey = aesKeyObj;
            //encrypt the private key
            return controller.asym.exec('encrypt', {text: keys.private_key});
        }).then((encryptedObj)=>{
            //send to the server
            controller.api.post('/api/groups/', {
                name: name,
                slug: slug,
                public_key: keys.public_key,
                encrypted_aes_key: aesKey.encrypted_key,
                encrypted_private_key: encryptedObj.ciphertext
            }).then((res)=>{
                res = res.json();
                //add the groups to the group db
                controller.db.groupDb.insert(res, (err, doc)=>{
                    //add the public key, aes_key and the private key
                    res.public_key = keys.public_keu;
                    res.encrypted_aes_key = aesKey.encrypted_key;
                    res.encrypted_private_key = encryptedObj.ciphertext;
                    if(err){
                        return reject(err);
                    }
                    return resolve(res);
                });
            }).catch((err)=>{
                return reject(err.json());
            });
        });
    });
};

/**
 * Add a single member to a group
 * @param {Controller} controller - the app controller
 * @param {string} member - the members email address
 * @param {number} groupId - the group id
 * @param {string} private_key - the groups private key
 * @param {groupUsers[]} accessgroupuser_set - the list of users in the group
 * @return {Promise}
 */
export var addMember = (controller:Controller, member:string, groupId:number, private_key:string, accessgroupuser_set:groupUsers[]): any =>{
    return new Promise((resolve, reject)=>{
        //look the member up
        controller.api.get(`/api/user-lookup/${member}/`).then((res)=>{
            res = res.json();
            /**
             * The users public key
             * @type {string}
             */
            var publicKey: string = res.public_key;
            /**
             * The users id
             * @type {number}
             */
            var id: number = res.id;

            /**
             * The length of the assgroupuser_set
             * @type {number}
             */
            var user_set_length = accessgroupuser_set.length;
            //check if the users allready exists
            for(var i=0; i<user_set_length; i++){
                if(accessgroupuser_set[i].user === id){
                    return resolve();
                }
            }

            /**
             * The new aes key for the group user
             * @type {string}
             */
            var aesKey: string;
            /**
             * The encrypted private key for the group user
             * @type {string}
             */
            var encPrivateKey: string;
            //generate the new aes key for the group user
            controller.asym.exec('make_aes_key').then(()=>{
                //encrypt the aes key using the users public key
                return controller.asym.exec('get_encrypted_aes_key', {public_key: publicKey, use_base64:true});
            }).then((aesKeyObj)=>{
                aesKey = aesKeyObj.encrypted_key;
                //encrypt the private key with the new aes key
                return controller.asym.exec('encrypt', {text: private_key});
            }).then((encryptedObj)=>{
                encPrivateKey = encryptedObj.ciphertext;
                //send to the server
                controller.api.post(`/api/groups/${groupId}/users/`,{
                    user: id,
                    is_group_admin: false,
                    encrypted_aes_key: aesKey,
                    encrypted_private_key: encPrivateKey
                }).then((res)=>{
                    //add the members to the local db
                    controller.db.groupDb.update({id: groupId}, {$addToSet: {members:member}}, {}, (err, numreplaced)=>{
                        if(err){
                            return reject(`Unable to save ${member}`);
                        }
                        return resolve();
                    });
                }).catch((err)=>{
                    return reject(`Unable to add ${member} to group`);
                });
            });
        }).catch((err)=>{
            return reject(`Member ${member} does not exist`);
        });
    });
};

/**
 * Add members to a group
 * @param {Controller} controller - the app controller
 * @param {string[]} members - the emails of the members in an array
 * @param {number} id - the group id
 * @return {Promise}
 */
export var addMembers = (controller:Controller, members:string[], id:number): any =>{
    return new Promise((resolve, reject)=>{
        //get the groups public and private key
        controller.api.get(`/api/groups/${id}/`).then((res)=>{
            res = res.json();
            /**
             * The group encryped aes key
             * @type {string}
             */
            var encryptedAesKey:string = res.my_encrypted_aes_key;
            /**
             * The encrypted private key
             * @type {string}
             */
            var encryptedPrivateKey:string = res.my_encrypted_private_key;
            /**
             * The unencrypted group private key
             * @type {string}
             */
            var privateKey: string;
            /**
             * The group public key
             * @type {string}
             */
            var publicKey:string = res.public_key;
            /**
             * The users in the group
             * @type {groupUsers}
             */
            var accessgroupuser_set:groupUsers[] = res.accessgroupuser_set;
            //decrypt the aes key
            controller.asym.exec('set_aes_key_from_encrypted', {aes_key: encryptedAesKey, use_base64: true}).then(()=>{
                //decrypt the group private key
                return controller.asym.exec('decrypt', {ciphertext: encryptedPrivateKey});
            }).then((rst)=>{
                privateKey = rst.text;

                /**
                 * The position in the members array
                 * @type {number}
                 */
                var position: number = 0;

                /**
                 * The length of the members array
                 * @type {number}
                 */
                var length: number = members.length;

                /**
                 * Add each member
                 */
                var addRecurse = () =>{
                    if(position === length){
                        controller.api.disableMulti();
                        return resolve();
                    }
                    addMember(controller, members[position], id, privateKey, accessgroupuser_set).then(()=>{
                        position++;
                        return addRecurse();
                    }).catch((err)=>{
                        controller.api.disableMulti();
                        return reject(err);
                    });
                };
                //enable multimode
                controller.api.enableMulti();
                addRecurse();
            });
        }).catch((err)=>{
            return reject('Unable to add members to group');
        });
    });
};
