
import Controller from '../controller/controller';

/**
 * Encrypt the users secrets
 * @param {Controller} controller - the app controller
 * @param {Object(key,value)} secrets - the secrets in an array of key value
 * @param {string} public_key - the public key to use for encrypting, if not specified use the users key
 * @return {Promise(encryptedKey, encryptedSecrets)} the encryptedKey and encryptedSecrets in an object
 */
export var client_encrypt_secrets = (controller:Controller, secrets:any, public_key?:string): any =>{
    return new Promise((resolve, reject)=>{
        /**
         * The object names of the secrets in array
         * @type {string[]}
         */
        var secretKeys:string[] = Object.keys(secrets);
        /**
         * The position in the secrets keys array
         * @type {number}
         */
        var position:number = 0;
        /**
         * The length of the secrets array
         * @type {number}
         */
        var length:number = secretKeys.length;
        /**
         * The encrytped aes key
         * @type {string}
         */
        var encryptedAesKey:string;
        /**
         * The encrypted secrets
         * @type {Object(key,value)}
         */
        var encryptedSecrets:any = {};
        /**
         * Recursivly encrypt the secrets
         */
        var recurseEncrypt:Function = ():void =>{
            if(position === length){
                return resolve({encrypted_key: encryptedAesKey, hidden_data: encryptedSecrets});
            }
            controller.asym.exec('encrypt', {text: secrets[secretKeys[position]]}).then((encObj)=>{
                encryptedSecrets[secretKeys[position]] = encObj.ciphertext;
                position++;
                return recurseEncrypt();
            });
        };
        if(public_key){
            //load the public key into the worker
            controller.asym.exec('set_public_key',{public_key: public_key}).then(()=>{
                //generate the new aes key
                return controller.asym.exec('make_aes_key');
            }).then((aesKeyObj)=>{
                //encrypt the aes key witht the public key
                return controller.asym.exec('rsa_encrypt', {text:aesKeyObj.aes_key, use_base64:true});
            }).then((encryptedObj)=>{
                encryptedAesKey = encryptedObj.ciphertext;
                //reload the users keys
                controller.user.load_keys();
                //encrypt
                recurseEncrypt();
            });
        } else {
            //use the users public key for encryption
            controller.asym.exec('make_aes_key').then((aesKeyObj)=>{
                //encrypt the aes key witht the public key
                return controller.asym.exec('rsa_encrypt', {text:aesKeyObj.aes_key, use_base64:true});
            }).then((encryptedObj)=>{
                encryptedAesKey = encryptedObj.ciphertext;
                //encrypt
                recurseEncrypt();
            });
        }
    });
};

/**
 * Send a new secret to the server and add it to the local db
 * @param {Controller} controller
 * @param {newSecret} secretData - the data to send to the server
 * @return {Promise}
 */
export var sendSecret = (controller:Controller, secretData:newSecret): any =>{
    return new Promise((resolve, reject)=>{
        controller.api.post('/api/secrets/', secretData).then((res)=>{
            res = res.json();
            controller.db.secretDb.insert(res, (err, doc)=>{
                return resolve();
            });
        }).catch((err)=>{
            return reject('Unable To Create Secret');
        });
    });
};
