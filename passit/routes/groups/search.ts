
import {Component, OnInit} from 'angular2/core';
import {Router} from 'angular2/router';
import {Control, ControlGroup} from 'angular2/common';
import Controller from '../../controller/controller';
import {searchControl, searchGroup} from '../../include/search';

@Component({
    template: require('../../template/groups/search.html')
})

export default class SearchGroup implements OnInit {

    /**
     * The search Control
     * @type {Control}
     */
    public groupControl: Control = searchControl;

    /**
     * The group seach control group
     * @type {ControlGroup}
     */
    public searchGroup: ControlGroup = searchGroup;


    constructor(private _router:Router, private _controller:Controller){
        
    }

    ngOnInit() {
        this.groupControl.updateValue('');
    }

    /**
     * Search the groups
     */
    public search(): void {
        if(this.searchGroup.valid){
            this._controller.groupSearch.emit({name:this.searchGroup.value.search});
        } else {
            this._controller.groupSearch.emit(null);
        }
    }

}
