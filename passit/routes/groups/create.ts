
import {Component} from 'angular2/core';
import {Router} from 'angular2/router';
import {Control, ControlGroup, Validators} from 'angular2/common';
import Controller from '../../controller/controller';
import {groupSlugValidator} from '../../include/validator';
import {createGroup, addMembers} from '../../security/groups';

@Component({
    template: require('../../template/groups/create.html')
})

export default class CreateGroup {

    /**
     * The new group name
     * @type {Control}
     */
    public newGroupNameControl: Control = new Control('', Validators.required);

    /**
     * The new group slug
     * @type {Control}
     */
    public newGroupSlugControl: Control = new Control('', groupSlugValidator);

    /**
     * The members list
     * @type {Control}
     */
    public membersControl: Control = new Control('');

    /**
     * The group create control group
     * @type {ControlGroup}
     */
    public createGroup: ControlGroup = new ControlGroup({
        name: this.newGroupNameControl,
        slug: this.newGroupSlugControl,
        members: this.membersControl
    });

    /**
     * Lock to prevent mutiple requests at once
     * @type {boolean}
     */
    private _lock: boolean = false;

    constructor(private _router:Router, private _controller:Controller){

    }

    /**
     * Create a new group
     */
    public createNewGroup(): void {
        if(this._lock){
            return
        }
        this._lock = true;
        //check if valid

        /**
         * The group name
         * @type {string}
         */
        var name:string;

        /**
         * The group slug
         * @type {string}
         */
        var slug:string;

        /**
         * The members in an array
         * @type{string[]}
         */
        var members:string[]; 
        
        if(this.createGroup.value.members === ''){
            members = [];
        } else {
            members = this.createGroup.value.members.replace(' ','').split(',');
        }
        //filter duplicate members
        members = members.filter((member, index)=>{
            return members.indexOf(member) === index;
        });
        
        if(!this.createGroup.valid){
            //check if invalid slug
            if(this.createGroup.value.name === '' || this.createGroup.value.slug !== ''){
                this._lock = false;
                return;
            }
            //convert the name to the slug
            name = this.createGroup.value.name;
            slug = name.replace(' ','-').toLowerCase();
        } else {
            name = this.createGroup.value.name;
            slug = this.createGroup.value.slug;
        }
        this._controller.message.showStatus('Please Wait', 'We are making the group now');
        //send to the server
        createGroup(this._controller, name, slug).then((res)=>{
            if(members.length > 0){
                //add members
                addMembers(this._controller, members, res.id).then(()=>{
                    this.clearNewGroup();
                    this._controller.message.showStatus('Group Created', `Group ${name} has been created`, 4000);
                    this._lock = false;
                    this._controller.groupSearch.emit(null);
                }).catch((err)=>{
                    this._controller.message.showStatus('Error', err, 4000);
                    this._lock = false;
                });
            } else {
                //reload by navigating to the groups page
                this._controller.message.showStatus('Group Created', `Group ${name} has been created`, 4000);
                this.clearNewGroup();
                this._lock = false;
                this._controller.groupSearch.emit(null);
            }
        }).catch((err)=>{
            this._lock = false;
            this._controller.message.showStatus('Error', 'Unable To Create Group', 4000);
        });
    }

    /**
     * Clear the new group fields
     */
    public clearNewGroup(): void {
        this.newGroupNameControl.updateValue('');
        this.newGroupSlugControl.updateValue('');
        this.membersControl.updateValue('');
    }

}
