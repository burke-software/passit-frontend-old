
import {Component, OnInit} from 'angular2/core';
import {Router, RouteParams, RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import {Control, ControlGroup, Validators} from 'angular2/common';
import Controller from '../controller/controller';
import SearchSecrets from './secrets/search';
import BrowseSecrets from './secrets/browse';
import CreateSecrets from './secrets/create';
import {parseQuery} from '../include/query';
import SecretSite from '../directives/secretSite'; 

@Component({
    template: require('../template/secrets.html'),
    directives: [ROUTER_DIRECTIVES, SecretSite]
})

@RouteConfig([
    { path:'/search', component: SearchSecrets, name:'Search', useAsDefault: true },
    { path:'/browse', component: BrowseSecrets, name:'Browse' },
    { path:'/create/...', component: CreateSecrets, name:'Create' }
])

export default class Secrets implements OnInit {

    /**
     * The list of the users secrets
     * @type {object}
     */
    public secrets: any;
    
    constructor(private _router:Router, private _params:RouteParams, private _controller:Controller){
        //subsribe to the event emitter
        this._controller.secretSearch.subscribe((query)=>{
            this._load(query);
        });
    }

    /**
     * Load the users secrets
     * @param {object|string} query - the query to load the secrets from
     */
    private _load(query?:any): void {
        if(query){
            if(typeof query === 'string'){
                query = parseQuery(query);
            }
            //load all secrets from the db
            if(query.name){
                query.name = new RegExp(query.name);
            }
            this._controller.db.secretDb.find(query, (err, docs)=>{
                this.secrets = docs;
            });
        } else {
            //load all secrets from the db
            this._controller.db.secretDb.find({}, (err, docs)=>{
                this.secrets = docs;
            });
        }


    }

    ngOnInit() {
        /**
         * The query paramaters from the router
         * @type {string}
         */
        var query:any = this._params.get('query');
        this._load(query);
    }

    /**
     * Track the secret changes
     * @param {number} index - the index of the secrets array
     * @param {object} secret - the individual secret
     * @return {number} the secret id
     */
    public trackBySecret(index:number, secret:any): number {
        return secret.id;
    }


}
