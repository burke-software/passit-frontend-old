
import {Component, OnInit} from 'angular2/core';
import {Control, ControlGroup} from 'angular2/common';
import {Router, RouteConfig, OnActivate} from 'angular2/router';
import Controller from '../controller/controller';
import {emailControl, passwordControl, formGroup, placeholder} from '../include/form';
import {signup} from '../security/signup';

@Component({
    template: require('../template/signup.html')
})

export default class Signup implements OnInit {

    /**
     * The email control
     * @type {Control}
     */
    private _emailControl: Control = emailControl;

    /**
     * The password control
     * @type {Control}
     */
    private _passwordControl: Control = passwordControl;

    /**
     * The form group
     * @type {Control}
     */
    public formGroup: ControlGroup = formGroup;

    /**
     * The placeholder
     * @type {placeHolder}
     */
    public placeholder: placeHolder = placeholder;

    /**
     * A lock to prevent mutiple signups
     * @type {boolean}
     */
    private _lock: boolean = false;

    constructor(private _router:Router, private _controller:Controller){

    }

    ngOnInit(){
        if(this._controller.user.authenticated()){
            this._router.navigate(['/Home']);
        }
    }

    /**
     * Sign the user up
     */
    public signup(): void {
        if(this._lock){
            return;
        }
        this._lock = true;
        if(!this.formGroup.valid){
            this._lock = false;
            return this._controller.message.showStatus('Invalid Signup', 'Please enter your email address and password <br /> Passwords must be at least 6 charaters', 4000); 
        }
        this._controller.message.showStatus('Please Wait', 'We are preforming the first time setup'); 
        signup(this._controller, this.formGroup.value.email, this.formGroup.value.password).then(()=>{
            this._controller.message.showStatus('Welcome', 'Please log in', 4000);
            this._router.navigate(['/Login']);
        }).catch((err)=>{
            this._controller.message.showStatus('Sign Up Error', err, 4000); 
            this._lock = false;
            this._emailControl.updateValue('');
            this._passwordControl.updateValue('');
        });
    }

};
