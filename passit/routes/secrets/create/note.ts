
import {Component} from 'angular2/core';
import {Router} from 'angular2/router';
import {Control, ControlGroup, Validators} from 'angular2/common';
import Controller from '../../../controller/controller';

@Component({
    template: require('../../../template/secrets/create/note.html')
})

export default class NewNote {

    constructor(private _router:Router, private _controller:Controller){

    }

}
