
import {Component, OnInit} from 'angular2/core';
import {Router} from 'angular2/router';
import {Control, ControlGroup, Validators} from 'angular2/common';
import Controller from '../../../controller/controller';
import {urlValidator} from '../../../include/validator';
import {client_encrypt_secrets, sendSecret} from '../../../security/secrets';

@Component({
    template: require('../../../template/secrets/create/site.html')
})

export default class NewSite implements OnInit {

    /**
     * The site name control
     * @type {Control}
     */
    public siteNameControl: Control = new Control('', Validators.required);

    /**
     * The url control
     * @type {Control}
     */
    public urlControl: Control = new Control('', urlValidator);

    /**
     * The username control
     * @type {Control}
     */
    public userNameControl: Control = new Control('', Validators.required);

    /**
     * The password control
     * @type {Control}
     */
    public passwordControl: Control = new Control('', Validators.required);

    /**
     * The create secrets
     * @type {ControlGroup}
     */
    public createSiteSecret: ControlGroup = new ControlGroup({
        name: this.siteNameControl,
        url: this.urlControl,
        userName: this.userNameControl,
        password: this.passwordControl,
    });

    /**
     * The list of groups loaded in from local storage
     * @type {array}
     */
    public groups: any;

    /**
     * Lock the form to prevent mutile submits
     * @type {boolean}
     */
    private _lock: boolean = false;


    constructor(private _router:Router, private _controller:Controller){

    }

    ngOnInit() {
        //load the groups from the local db
        this._controller.db.groupDb.find({}, (err, docs)=>{
            this.groups = docs;
        });

        //select2
        $(".foo").select2();
    }

    /**
     * Create a new website secret
     */
    public createNewSiteSecret(): void {
        if(this._lock || !this.createSiteSecret.valid){
            return;
        }
        this._lock = true;
        /**
         * The groups we are adding the secret to
         * @type {array}
         */
        var addToGroups:string[] = $(".foo").val();

        /**
         * The data we are sending to the server
         * @type {newSecret}
         */
        var secretObject:newSecret = {
            name: this.createSiteSecret.value.name,
            type: 'website',
            visible_data: {
                url: this.createSiteSecret.value.url
            },
            secrets: []
        };

        this._controller.message.showStatus('Please Wait', 'We are generating the secret now');

        /**
         * The secrets to be encrypted
         * @type {Object}
         */
        var plainSecrets:any = {
            username: this.createSiteSecret.value.userName,
            password: this.createSiteSecret.value.password
        };


        /**
         * Encrypt a clients secrets
         */
        var encryptClientSecret:Function = () =>{
            client_encrypt_secrets(this._controller, plainSecrets).then((encryptedData)=>{
                secretObject.secrets.push(encryptedData);
                //send to the server
                sendSecret(this._controller, secretObject).then((res)=>{
                    this._controller.message.showStatus('Created', `Secret ${this.createSiteSecret.value.name} has been created`, 4000); 
                    this._lock = false;
                    this.clearNewSecret();
                    this._controller.secretSearch.emit(null);
                }).catch((err)=>{
                    this._controller.message.showStatus('Error', err, 4000);
                    this._lock = false;
                });
            });
        };

        /**
         * Add a group to the secrets
         */
        var addGroupSecrets:Function = () =>{
            if(addToGroups.length === 0){
                //send to the server
                sendSecret(this._controller, secretObject).then((res)=>{
                    this._controller.message.showStatus('Created', `Secret ${this.createSiteSecret.value.name} has been created`, 4000); 
                    this._lock = false;
                    this.clearNewSecret();
                    this._controller.secretSearch.emit(null);
                }).catch((err)=>{
                    this._controller.message.showStatus('Error', err, 4000);
                    this._lock = false;
                });
            }
            var groupId = addToGroups.shift();
            //get the group public key
            this._controller.db.groupDb.findOne({id: groupId}, (err, doc)=>{
                client_encrypt_secrets(this._controller, plainSecrets, doc.public_key).then((encryptedData)=>{
                    encryptedData.group = groupId;
                    secretObject.secrets.push(encryptedData);
                    addGroupSecrets();
                });
            });
        };

        //if we have groups get the group keys
        if(addToGroups){
            addGroupSecrets();
        } else {
            //just encrypt for the user
            encryptClientSecret();
        }

    }

    /**
     * Clear the new secret form
     */
    public clearNewSecret(): void {
        this.siteNameControl.updateValue('');
        this.urlControl.updateValue('');
        this.userNameControl.updateValue('');
        this.passwordControl.updateValue('');
    }

}
