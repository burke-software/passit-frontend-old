
import {Component, OnInit} from 'angular2/core';
import {Router} from 'angular2/router';
import {Control, ControlGroup} from 'angular2/common';
import Controller from '../../controller/controller';
import {searchControl, searchGroup} from '../../include/search';

@Component({
    template: require('../../template/secrets/search.html')
})

export default class SearchSecrets implements OnInit {

    /**
     * The search control
     * @type {Control}
     */
    public secretControl: Control = searchControl;

    /**
     * The search control group
     * @type {ControlGroup}
     */
    public searchSecret: ControlGroup = searchGroup;


    constructor(private _router:Router, private _controller:Controller){

    }

    ngOnInit() {
        this.secretControl.updateValue('');
    }

    /**
     * Search the secrets
     */
    public search(): void {
        if(this.searchSecret.valid){
            this._controller.secretSearch.emit({name:this.searchSecret.value.search});
        } else {
            this._controller.secretSearch.emit(null);
        }
    }

}

