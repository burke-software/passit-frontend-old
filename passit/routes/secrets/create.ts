
import {Component} from 'angular2/core';
import {Router, RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import Controller from '../../controller/controller';
import NewSite from './create/site';
import NewNote from './create/note';
import {urlValidator} from '../../include/validator';

@Component({
    template: require('../../template/secrets/create.html'),
    directives: [ROUTER_DIRECTIVES]
})

@RouteConfig([
    { path:'/site', component: NewSite, name:'Site', useAsDefault: true },
    { path:'/note', component: NewNote, name:'Note' }
])

export default class CreateSecrets {

    constructor(private _router:Router, private _controller:Controller){

    }

}
