
import {Component, OnInit} from 'angular2/core';
import {Router} from 'angular2/router';
import {Control, ControlGroup, Validators} from 'angular2/common';
import Controller from '../../controller/controller';

@Component({
    template: require('../../template/secrets/browse.html')
})

export default class BrowseSecrets implements OnInit{

    constructor(private _router:Router, private _controller:Controller){

    }

    ngOnInit() {
        //set up select2
        $(".browse-groups").select2({
            placeholder: "Click to filter"
        });
        $(".browse-tags").select2({
            placeholder: "Click to filter"
        });
        $(".browse-types").select2({
            placeholder: "Click to filter"
        });
    }

}
