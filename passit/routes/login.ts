
import {Component, OnInit} from 'angular2/core';
import {Router, RouteConfig, OnActivate} from 'angular2/router';
import {Control, ControlGroup} from 'angular2/common';
import Controller from '../controller/controller';
import {emailControl, passwordControl, formGroup, placeholder} from '../include/form';
import {login} from '../security/login';

@Component({
    template: require('../template/login.html')
})

export default class Login implements OnInit {

    /**
     * The form group
     * @type {Control}
     */
    public formGroup: ControlGroup = formGroup;

    /**
     * The placeholder
     * @type {placeHolder}
     */
    public placeholder: placeHolder = placeholder;

    constructor(private _router:Router, private _controller:Controller){

    }

    ngOnInit(){
        if(this._controller.user.authenticated()){
            this._router.navigate(['/Home']);
        }
    }

    /**
     * Log the user in
     */
    public login(): void {
        login(this._controller, this.formGroup.value.email, this.formGroup.value.password, this.formGroup.value.rememberMe).then(()=>{
            this._router.navigate(['/Home']);
        }).catch(()=>{
            this._controller.message.showStatus('Login Error', 'Unable to login', 4000);
        });
    }

};
