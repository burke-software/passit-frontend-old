
import {Component, OnInit} from 'angular2/core';
import {Router, RouteParams, RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import Controller from '../controller/controller';
import CreateGroup from './groups/create';
import SearchGroup from './groups/search';
import {parseQuery} from '../include/query';
import GroupItem from '../directives/groupItem'; 

@Component({
    template: require('../template/groups.html'),
    directives: [ROUTER_DIRECTIVES, GroupItem]
})

@RouteConfig([
    { path:'/search', component: SearchGroup, name:'Search', useAsDefault: true },
    { path:'/create', component: CreateGroup, name:'Create'}
])

export default class Groups implements OnInit {

    /**
     * The users groups
     * @type {any}
     */
    public groups:any;

    constructor(private _router:Router, private _params:RouteParams, private _controller:Controller){
        //subsribe to the event emitter
        this._controller.groupSearch.subscribe((query)=>{
            this._load(query);
        });
    }

    /**
     * Load the groups based on a query
     * @param {object|string} query - the search query
     */
    private _load(query?:any): void {
        if(query){
            if(typeof query === 'string'){
                query = parseQuery(query);
            }
            if(query.name){
                query.name = new RegExp(query.name);
            }
            this._controller.db.groupDb.find(query, (err, docs)=>{
                this.groups = docs;
            });
        } else {
            //load the default groups from the db
            this._controller.db.groupDb.find({}, (err, docs)=>{
                this.groups = docs;
            });
        }
    }

    /**
     * On Load Check if query
     */
    ngOnInit() {
        /**
         * The query paramaters from the router
         * @type {string}
         */
        var query:any = this._params.get('query');
        this._load(query);
    }

    /**
     * Track the group changes
     * @param {number} index - the index of the group array
     * @param {object} group - the current group
     * @return {number} the group id
     */
    public trackByGroup(index:number, group:any): number {
        return group.id;
    }
    

}
