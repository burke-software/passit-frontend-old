
import {Component} from 'angular2/core';
import {Router, RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import Controller from '../controller/controller';
import Secrets from './secrets';
import Groups from './groups';

@Component({
    template: require('../template/home.html'),
    directives: [ROUTER_DIRECTIVES]
})

@RouteConfig([
    {path: '/secrets/...', component: Secrets, name: 'Secret', useAsDefault: true},
    {path: '/secrets/:query/...', component: Secrets, name: 'Secrets'},
    {path: '/groups/...', component: Groups, name: 'Group'},
    {path: '/groups/:query/...', component: Groups, name: 'Groups'}
])

export default class Home {

    constructor(private _router:Router, private _controller:Controller){

    }

};
