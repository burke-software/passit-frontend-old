
import {Component, Input, OnInit} from 'angular2/core';
import {Control, ControlGroup, Validators} from 'angular2/common';
import Controller from '../controller/controller';
import {urlValidator} from '../include/validator';

@Component({
    selector: 'secret-site',
    template: require('../template/directives/secretSite.html')
})

export default class SecretItem implements OnInit {

    /**
     * The secret
     * @type {object}
     */
    @Input() public item:any;

    /**
     * The site name control
     * @type {Control}
     */
    public siteNameControl:Control = new Control('', Validators.required);

    /**
     * The url control
     * @type {Control}
     */
    public urlControl:Control = new Control('', urlValidator);

    /**
     * The username control
     * @type {Control}
     */
    public userNameControl:Control = new Control('', Validators.required);

    /**
     * The password control
     * @type {Control}
     */
    public passwordControl:Control = new Control('', Validators.required);

    constructor(private _controller:Controller){

    }

    ngOnInit() {
        this.siteNameControl.updateValue(this.item.name);
        this.urlControl.updateValue(this.item.visible_data.url);
    }

}
