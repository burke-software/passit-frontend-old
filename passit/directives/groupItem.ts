
import {Component, Input, OnInit} from 'angular2/core';
import {Control, ControlGroup, Validators} from 'angular2/common';
import Controller from '../controller/controller';
import {groupSlugValidator} from '../include/validator';

@Component({
    selector: 'group-item',
    template: require('../template/directives/groupItem.html')
})

export default class GroupItem implements OnInit {

    /**
     * The group for the directive
     * @type {object}
     */
    @Input() public item: any;

    /**
     * The group name control
     * @type {Control}
     */
    public groupNameControl:Control = new Control('', Validators.required);

    /**
     * The group slug control
     * @type {Control}
     */
    public groupSlugControl:Control = new Control('', groupSlugValidator);

    /**
     * The group control group
     * @type {ControlGroup}
     */
    public formGroup:ControlGroup = new ControlGroup({
        name: this.groupNameControl,
        slug: this.groupSlugControl
    });

    constructor(private _controller:Controller){

    }

    ngOnInit() {
        this.groupNameControl.updateValue(this.item.name);
        this.groupSlugControl.updateValue(this.item.slug);
        //load the groups into select2
        setTimeout(()=>{
            $(`#mulit-${ this.item.name }-${ this.item.id }`).select2();
        },10);
    }

}
