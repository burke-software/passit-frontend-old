
/**
 * Scss and css
 */
import './node_modules/bootstrap/dist/css/bootstrap.min.css';
import './lib/select2/css/select2.min.css';
import './lib/select2/css/select2-bootstrap.min.css';
import './lib/scss/styles.scss';

/**
 * Js
 */
import './node_modules/angular2/bundles/angular2-polyfills.js';
import './node_modules/bootstrap/dist/js/bootstrap.min.js';
import './lib/select2/js/select2.min.js';


import {provide} from 'angular2/core';
import {bootstrap} from 'angular2/platform/browser';
import {ROUTER_PROVIDERS} from 'angular2/router';
import {LocationStrategy, HashLocationStrategy} from 'angular2/platform/common';
import {HTTP_PROVIDERS} from 'angular2/http';
import Passit from './passit/passit';
import Controller from './passit/controller/controller';
import Master from 'simple-asymmetric-javascript';
import Db from './passit/controller/db';
import User from './passit/controller/user';
import Api from './passit/controller/api';
import Message from './passit/controller/message';

bootstrap(Passit, [ROUTER_PROVIDERS, provide(LocationStrategy, {useClass: HashLocationStrategy}), Controller, Master, Db, User, Api, HTTP_PROVIDERS, Message]);
